package com.zuitt.example;

import java.util.Scanner;
public class UserInput {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        String firstname = myObj.next();

        System.out.println("Last Name:");
        String lastname = myObj.next();

        System.out.println("First Subject Grade:");
        double firstSub = myObj.nextDouble();
        System.out.println("First Subject Grade:");
        double secondSub = myObj.nextDouble();
        System.out.println("First Subject Grade:");
        double thirdSub = myObj.nextDouble();

        System.out.println("Good day, " + firstname + " " + lastname + ".");
        System.out.println("Your grade average is: " + (firstSub + secondSub + thirdSub) / 3);

    }
}
